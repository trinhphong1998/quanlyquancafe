﻿using QuanLyQuanCaPhe.DAO;
using QuanLyQuanCaPhe.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyQuanCaPhe
{
    public partial class fAdmin : Form
    {
        BindingSource foodList = new BindingSource();

        BindingSource accountList = new BindingSource();

        BindingSource tableList = new BindingSource();

        public Account loginAccount;
        public fAdmin()
        {
            InitializeComponent();
            LoadData();
            this.pbLoading.Visible = false;
            cbExportExcel.SelectedIndex = 0;
            this.dtgvAccount.Columns["userName"].HeaderText = "Tên đăng nhập";
            this.dtgvAccount.Columns["displayname"].HeaderText = "Tên hiển thị";
            this.dtgvAccount.Columns["typeName"].HeaderText = "Loại tài khoản";
            this.dtgvAccount.Columns["type"].Visible = false;
            this.dtgvFood.Columns["id"].DisplayIndex = 0;
            this.dtgvFood.Columns["CategoryName"].HeaderText = "Danh mục";
            this.dtgvFood.Columns["CategoryName"].DisplayIndex = 1;
            this.dtgvFood.Columns["CategoryId"].Visible = false;
            this.dtgvFood.Columns["name"].HeaderText = "Tên";
            this.dtgvFood.Columns["price"].HeaderText = "Giá";
            this.dtgvTable.Columns["name"].HeaderText = "Tên bàn";
            this.dtgvTable.Columns["status"].HeaderText = "Trạng thái";
            this.dtgvTable.Columns["status"].DisplayIndex = 3;
            this.dtgvTable.Columns["name"].DisplayIndex = 0;
            this.dtgvTable.Columns["id"].DisplayIndex = 0;
            
            
            this.dtgvTable.Columns["statusid"].Visible = false;
            
        }
        #region methods

        
        void LoadData()
        {

            dtgvFood.DataSource = foodList;
            dtgvAccount.DataSource = accountList;
            dtgvTable.DataSource = tableList;

            LoadDateTimePickerBill();
            LoadListBillByDate(dtpkFromDate1.Value, dtpkToDate1.Value);
            LoadListFood();     
            LoadAccount();
            LoadTable();
            LoadCategoryIntoCombobox(cbCategory);
            AddFoodBinding();
            AddTableBiding();
            AddAccountBinding();
             
        }

        void AddAccountBinding()
        {
            txbUserName.DataBindings.Add(new Binding("Text", dtgvAccount.DataSource, "UserName", true, DataSourceUpdateMode.Never));
            txbDisplayName.DataBindings.Add(new Binding("Text", dtgvAccount.DataSource, "DisplayName", true, DataSourceUpdateMode.Never));
            //numericUpDown1.DataBindings.Add(new Binding("Value", dtgvAccount.DataSource, "Type", true, DataSourceUpdateMode.Never));// k chuyển đổi từ txb về datagriviewm
            cbType.DataBindings.Add(new Binding("SelectedIndex",dtgvAccount.DataSource,"Type"));
            
        }

        void LoadAccount()
        {
            accountList.DataSource = AccountDAO.Instance.GetListAccount();
        }
        void LoadTable()
        {
            tableList.DataSource = TableDAO.Instance.LoadTableList();
        }
        void LoadDateTimePickerBill()
        {
            DateTime today = DateTime.Now;
            dtpkFromDate1.Value = new DateTime(today.Year, today.Month, 1);
            dtpkToDate1.Value = dtpkFromDate1.Value.AddMonths(1).AddDays(-1);
        }
        void LoadListBillByDate(DateTime checkIn, DateTime checkOut)
        {
            dtgvBill1.DataSource = BillDAO.Instance.GetBillListByDate(checkIn, checkOut);
        }
        void AddFoodBinding()
        {
            txbFoodName.DataBindings.Add(new Binding("Text", dtgvFood.DataSource, "Name", true, DataSourceUpdateMode.Never));
            txbFoodID.DataBindings.Add(new Binding("Text", dtgvFood.DataSource, "ID", true, DataSourceUpdateMode.Never));
            nmFoodPrice.DataBindings.Add(new Binding("Value", dtgvFood.DataSource, "Price", true, DataSourceUpdateMode.Never));

        }
        void AddTableBiding()
        {
            this.txtTableId.DataBindings.Add(new Binding("Text", dtgvTable.DataSource, "Id", true, DataSourceUpdateMode.Never));
            this.txtTableName.DataBindings.Add(new Binding("Text", dtgvTable.DataSource, "Name", true, DataSourceUpdateMode.Never));
            this.cbTableStatus.DataBindings.Add(new Binding("SelectedIndex", dtgvTable.DataSource, "statusId", true, DataSourceUpdateMode.Never));
        }   
        void LoadCategoryIntoCombobox(ComboBox cb)
        {
            cb.DataSource = CategoryDAO.Instance.GetListCategory();
            cb.DisplayMember = "Name";
        }
        void LoadListFood()
        {
            foodList.DataSource = FoodDAO.Instance.GetListFood();
        }

        void AddAccount(string userName, string displayName, int type)
        {
            if (AccountDAO.Instance.InsertAccount(userName, displayName, type))
            {
                MessageBox.Show("Thêm tài khoản thành công");
            }
            else
            {
                MessageBox.Show("Thêm tài khoản thất bại");
            }

            LoadAccount();
        }

        void EditAccount(string userName, string displayName, int type)
        {
            if (AccountDAO.Instance.UpdateAccount(userName, displayName, type))
            {
                MessageBox.Show("Cập nhật tài khoản thành công");
            }
            else
            {
                MessageBox.Show("Cập nhật tài khoản thất bại");
            }

            LoadAccount();
        }
        void EditTable(Table table)
        {
           if(TableDAO.Instance.EditTable(table))
           {
                MessageBox.Show("Cập nhật bàn thành công");
           }
           else
            {
                MessageBox.Show("Cập nhật bàn thất bại");
            }
            LoadTable();
        }
        void AddTable(Table table)
        {
            if(TableDAO.Instance.AddTable(table))
            {
                MessageBox.Show("Thêm bàn thành công");
            }    
            else
            {
                MessageBox.Show("Thêm bàn thất bại");
            }
            LoadTable();
        }
        void DeleteTable(int id)
        {
            if(TableDAO.Instance.DeleteTable(id))
            {
                MessageBox.Show("Đã xóa bàn thành công");
            }    
            else
            {
                MessageBox.Show("Xóa bàn thất bại");
            }
            LoadTable();
        }
        void DeleteAccount(string userName)
        {
            if (loginAccount.UserName.Equals(userName))
            {
                MessageBox.Show("Vui lòng không xóa tài khoản Admin");
                return;
            }
            
            if (AccountDAO.Instance.DeleteAccount(userName))
            {
                MessageBox.Show("Xóa tài khoản thành công");
            }
            else
            {
                MessageBox.Show("Xóa tài khoản thất bại");
            }

            LoadAccount();
        }

        void ResetPass(string userName)
        {
            if (AccountDAO.Instance.ResetPassword(userName))
            {
                MessageBox.Show("Đã đặt lại mật khẩu thành công");
            }
            else
            {
                MessageBox.Show("Đặt lại mật khẩu thất bại");
            }
            
        }
        #endregion

        #region events

        private void btnAddAccount_Click(object sender, EventArgs e)
        {
            string userName = txbUserName.Text;
            string displayName = txbDisplayName.Text;
            //int type = (int)numericUpDown1.Value;
            int type = (int)this.cbType.SelectedIndex;
            AddAccount(userName, displayName, type);
        }

        private void btnDeleteAccount_Click(object sender, EventArgs e)
        {
            string userName = txbUserName.Text;

            DeleteAccount(userName);
        }

        private void btnEditAccount_Click(object sender, EventArgs e)
        {
            string userName = txbUserName.Text;
            string displayName = txbDisplayName.Text;
            //int type = (int)numericUpDown1.Value;
            int type = this.cbType.SelectedIndex;
            EditAccount(userName, displayName, type);
        }

        private void btnResetPassword_Click(object sender, EventArgs e)
        {
            string userName = txbUserName.Text;
            ResetPass(userName);
        }

        private void btnShowAccount_Click(object sender, EventArgs e)
        {
            LoadAccount();
        }
        
        private void txbFoodID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (dtgvFood.SelectedCells.Count > 0)
                {
                    int id = (int)dtgvFood.SelectedCells[0].OwningRow.Cells["CategoryID"].Value;

                    Category cateogory = CategoryDAO.Instance.GetCategoryByID(id);

                    cbCategory.SelectedItem = cateogory;

                    int index = -1;
                    int i = 0;
                    foreach (Category item in cbCategory.Items)
                    {
                        if (item.ID == cateogory.ID)
                        {
                            index = i;
                            break;
                        }
                        i++;
                    }

                    cbCategory.SelectedIndex = index;
                }
            }
            catch { }
        }

        private void btnAddFood_Click(object sender, EventArgs e)
        {
            AddOrEditMenu addOrEditMenu = new AddOrEditMenu(null);
            addOrEditMenu.Show();
            //string name = txbFoodName.Text;
            //int categoryID = (cbCategory.SelectedItem as Category).ID;
            //float price = (float)nmFoodPrice.Value;

            //if (FoodDAO.Instance.InsertFood(name, categoryID, price))
            //{
            //    MessageBox.Show("Đã thêm món mới thành công");
            //    LoadListFood();
            //    if (insertFood != null)
            //        insertFood(this, new EventArgs());

            //}
            //else
            //{
            //    MessageBox.Show("Có lỗi khi thêm thức ăn");
            //}
        }

        private void btnEditFood_Click(object sender, EventArgs e)
        {
            string name = txbFoodName.Text;
            int categoryID = (cbCategory.SelectedItem as Category).ID;
            float price = (float)nmFoodPrice.Value;
            int id = Convert.ToInt32(txbFoodID.Text);
            //this.Hide();
            Food food = new Food(id, name, categoryID, price);
            //food.CategoryID = categoryID;
            //Food food = new Food(id, name, categoryID, price);
            new AddOrEditMenu(food).Show();
            
            //if (FoodDAO.Instance.UpdateFood(id, name, categoryID, price))
            //{
            //    MessageBox.Show("Đã sửa món thành công");
            //    LoadListFood();
            //    if (updateFood != null)
            //        updateFood(this, new EventArgs());

            //}
            //else
            //{
            //    MessageBox.Show("Có lỗi khi sửa món ăn");
            //}
        }

        private void btnDeleteFood_Click(object sender, EventArgs e)
        {
            string name = txbFoodName.Text;
            int categoryID = (cbCategory.SelectedItem as Category).ID;
            int id = Convert.ToInt32(txbFoodID.Text);

            if (FoodDAO.Instance.DeleteFood(id))
            {
                MessageBox.Show("Đã xóa món thành công");
                LoadListFood();
                if (deleteFood != null)
                    deleteFood(this, new EventArgs());
            }
            else
            {
                MessageBox.Show("Có lỗi khi xóa thức ăn");
            }
        }
        private void btnShow_Click(object sender, EventArgs e)
        {
            LoadListFood();
        }
        private void btnViewBill_Click(object sender, EventArgs e)
        {
            LoadListBillByDate(dtpkFromDate1.Value, dtpkToDate1.Value);
        }
    
        private void btnViewBill1_Click(object sender, EventArgs e)
        {
            LoadListBillByDate(dtpkFromDate1.Value, dtpkToDate1.Value);

        }
        private void btnViewBill2_Click(object sender, EventArgs e)
        {
            this.pbLoading.Visible = true;

            if (cbExportExcel.SelectedIndex == 0)
            {

                BillDAO.Instance.ExportToExcelByDay();

            }
            if (cbExportExcel.SelectedIndex == 1)
            {

                BillDAO.Instance.ExportToExcelByMonth();

            }
            this.pbLoading.Visible = false;
        }
        private event EventHandler insertFood;
        public event EventHandler InsertFood
        {
            add { insertFood += value; }
            remove { insertFood -= value; }
        }

        private event EventHandler deleteFood;
        public event EventHandler DeleteFood
        {
            add { deleteFood += value; }
            remove { deleteFood -= value; }
        }

        private event EventHandler updateFood;
        public event EventHandler UpdateFood
        {
            add { updateFood += value; }
            remove { updateFood -= value; }
        }
        private void fAdmin_Load(object sender, EventArgs e)
        {


        }
        #endregion

        private void dtgvAccount_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(this.txtTableId.Text);
            string name = this.txtTableName.Text;
            string status = this.cbTableStatus.Text;
            Table table = new Table(id, name, status);
            AddTable(table);
        }

        private void BtnEditTable_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(this.txtTableId.Text);
            string name = this.txtTableName.Text;
            string status = this.cbTableStatus.Text;
            Table table = new Table(id, name, status);
            EditTable(table);
        }

        private void BtnDeleteTable_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(this.txtTableId.Text);
            DeleteTable(id);
        }
    }
}
