﻿using QuanLyQuanCaPhe.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyQuanCaPhe.DAO
{
    public class TableDAO
    {
        private static TableDAO instance;

        public static TableDAO Instance
        {
            get { if (instance == null) instance = new TableDAO(); return TableDAO.instance; }
            private set { TableDAO.instance = value; }
        }

        public static int TableWidth = 100;
        public static int TableHeight = 100;

        private TableDAO() { }

        public void SwitchTable(int id1, int id2)
        {
            DataProvider.Instance.ExecuteQuery("USP_SwitchTable @idTable1 , @idTable2", new object[] { id1, id2 });
        }

        public List<Table> LoadTableList()
        {
            List<Table> tableList = new List<Table>();

            //DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetTableList");
            string query = @"SELECT id, name, status ,
                            CASE
                                WHEN status = N'Trống'  THEN 0
                                WHEN status = N'Có người' THEN 1
                            END AS statusid
                            FROM dbo.TableFood";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            foreach (DataRow item in data.Rows)
            {
                Table table = new Table(item);
                tableList.Add(table);
            }

            return tableList;
        }

        public bool AddTable(Table table)
        {
            string query = String.Format("insert into TableFood (name, status) VALUES  ( N'{0}', N'{1}')", table.Name, table.Status);
            DataProvider.Instance.ExecuteNonQuery(query);
            return true;
        }
        public bool EditTable(Table table)
        {
            string query = String.Format("update TableFood set name = N'{0}', status = N'{1}' where id = {2}", table.Name, table.Status, table.ID);
            DataProvider.Instance.ExecuteNonQuery(query);
            return true;
        }
        public bool DeleteTable(int id)
        {
            string query = String.Format("delete from TableFood where id = {0}",id);
            DataProvider.Instance.ExecuteNonQuery(query);
            return true;
        }
        public Table GetTable(int id)
        {
            Table table = null;
            string query = @"SELECT id, name, status ,
                        CASE
                            WHEN status = N'Trống'  THEN 0
                            WHEN status = N'Có người' THEN 1
                        END AS statusid
                        FROM dbo.TableFood where id = " + id;
            var data = DataProvider.Instance.ExecuteQuery(query);
            foreach (DataRow item in data.Rows)
            {
                table = new Table(item);
            }
            return table;
        }
    }
}
