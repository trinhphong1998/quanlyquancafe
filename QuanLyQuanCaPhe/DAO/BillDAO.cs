﻿using QuanLyQuanCaPhe.DTO;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Description;

namespace QuanLyQuanCaPhe.DAO
{
    public class BillDAO
    {
        private static BillDAO instance;

        public static BillDAO Instance
        {
            get { if (instance == null) instance = new BillDAO(); return BillDAO.instance; }
            private set { BillDAO.instance = value; }
        }

        private BillDAO() { }
        // Thành công: bill ID
        // Thất bại: -1
        public int GetUncheckBillIDByTableTD(int id)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT * FROM dbo.Bill WHERE idTable = "+ id +" AND status = 0");

            if (data.Rows.Count > 0)
            {
                Bill bill = new Bill(data.Rows[0]);
                return bill.ID;
            }

            return -1;
        }

        public void CheckOut(int id, int discount, float totalPrice)
        {
            string query = "UPDATE dbo.Bill SET dateCheckOut = GETDATE(), status = 1, " + " discount = " + discount + ", totalPrice = " + totalPrice + " WHERE id = " + id;
            DataProvider.Instance.ExecuteNonQuery(query);
        }

        public void InsertBill(int id)
        {
            DataProvider.Instance.ExecuteNonQuery("exec USP_InsertBill @idTable", new object[]{id});
        }

        public DataTable GetBillListByDate(DateTime checkIn, DateTime checkOut)
        {
            return DataProvider.Instance.ExecuteQuery("exec USP_GetListBillByDate @checkIn , @checkOut", new object[]{checkIn, checkOut});
        }

        public int GetMaxIDBill()
        {
            try
            {
                return (int)DataProvider.Instance.ExcuteScalar("SELECT MAX(id) FROM dbo.Bill");
            }
            catch
            {
                return 1;
            }
        }
        private void ExportToExcel(string query, string fileName)
        {
            DataTable dataTable = DataProvider.Instance.ExecuteQuery(query);
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2016;

                //Create a new workbook
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet sheet = workbook.Worksheets[0];

                //Create a dataset from XML file


                //Import data from the data table with column header, at first row and first column, 
                //and by its column type.
                sheet.ImportDataTable(dataTable, true, 1, 1, true);

                //Creating Excel table or list object and apply style to the table
                IListObject table = sheet.ListObjects.Create("Employee_PersonalDetails", sheet.UsedRange);

                table.BuiltInTableStyle = TableBuiltInStyles.TableStyleMedium14;

                //Autofit the columns
                sheet.UsedRange.AutofitColumns();

                //Save the file in the given path
                String path = "../../excel/" + fileName + ".xlsx";
                string fullPath = Path.GetFullPath(path);
                Stream excelStream = File.Create(fullPath);
                workbook.SaveAs(excelStream);
                excelStream.Dispose();
                System.Windows.Forms.MessageBox.Show("Đã xuất ra file " + fullPath);
            }
        }
        public void ExportToExcelByDay()
        {
                             //DateCheckOut AS [Ngày ra],
            string query = @"SELECT
                            DateCheckIn AS [Ngày Tháng Năm], 
                           
                            sum(b.totalPrice) AS [Tổng Doanh Thu] 
                            FROM dbo.Bill AS b
                            group by DateCheckOut,DateCheckIn ";
            this.ExportToExcel(query, "SumByDay");

        }
        public void ExportToExcelByMonth()
        {
            string query = @"select YEAR(DateCheckOut) as [Năm],
                            MONTH(DateCheckOut) [Tháng] ,
                            COUNT(1) [Doanh số HĐ], 
                            sum(totalPrice) as [Tổng Doanh Thu]
                            from dbo.Bill GROUP BY YEAR(DateCheckOut), 
                            MONTH(DateCheckOut), 
                            DATENAME(MONTH, DateCheckOut)";
            this.ExportToExcel(query, "ExportByMonth");
        }
    }
}
