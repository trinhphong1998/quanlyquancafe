﻿using QuanLyQuanCaPhe.DAO;
using QuanLyQuanCaPhe.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyQuanCaPhe
{
    public partial class AddOrEditMenu : Form
    {
        private Food food = null;
        public AddOrEditMenu(DTO.Food food)
        {
            this.food = food;
            InitializeComponent();
            this.cbCategory.DataSource = CategoryDAO.Instance.GetListCategory();
            this.cbCategory.DisplayMember = "Name";
            if (food == null)
            {
                this.lbTitle.Text = "Thêm thực đơn";
                this.btnAddFood.Text = "Thêm";
            }else
            {
                this.lbTitle.Text = "Sửa thực đơn";
                this.btnAddFood.Text = "Cập nhật";
                this.txbFoodID.Enabled = false;
                this.txbFoodName.Text = food.Name;
                //this.cbCategory.SelectedItem = CategoryDAO.Instance.GetCategoryByID(food.CategoryID);
                Category cateogory = CategoryDAO.Instance.GetCategoryByID(food.CategoryID);
                //cbCategory.SelectedItem = cateogory;
                this.cbCategory.SelectedIndex = food.CategoryID-1;
                this.txbFoodID.Text = food.ID.ToString();
                this.nmFoodPrice.Value = (int)food.Price;
            }    
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnAddFood_Click(object sender, EventArgs e)
        {
            if(this.food == null)
            {
                string name = txbFoodName.Text;
                int categoryID = (cbCategory.SelectedItem as Category).ID;
                float price = (float)nmFoodPrice.Value;

                if (FoodDAO.Instance.InsertFood(name, categoryID, price))
                {
                    MessageBox.Show("Đã thêm món mới thành công");
                    //LoadListFood();
                    //if (insertFood != null)
                    //    insertFood(this, new EventArgs());

                }
                else
                {
                    MessageBox.Show("Có lỗi khi thêm thức ăn");
                }
            }else
            {
                string name = txbFoodName.Text;
                int categoryID = (cbCategory.SelectedItem as Category).ID;
                float price = (float)nmFoodPrice.Value;
                int id = Convert.ToInt32(txbFoodID.Text);

                if (FoodDAO.Instance.UpdateFood(id, name, categoryID, price))
                {
                    MessageBox.Show("Đã sửa món thành công");
                    //LoadListFood();
                    //if (updateFood != null)
                    //    updateFood(this, new EventArgs());

                }
                else
                {
                    MessageBox.Show("Có lỗi khi sửa món ăn");
                }
            }
            this.Close();
            fAdmin fAdmin = new fAdmin();
            fAdmin.tcBill.SelectedIndex = 1;
            fAdmin.Show();
        }
    }
}
